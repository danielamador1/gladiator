# Gladiator Trainer REST Service

## Dependencies
- Mysql
- Maven
- Java
- Apache Tomcat

## How to run (from terminal)
1. git clone repository
2. Set your mysql instance to default port (3306) and username/password to root/password, respectively.
2.1 If you want to forgo this you will have to change these values to correspond to yours in the AppConfig.java, application.properties, and persistence.xml files, respectively
2. mvn clean install (It should already be set up to create WAR files)
3. search for WAR file in "/target" directory
4. Copy and paste to your {Tomcat}/webapps directory
5. Restart tomcat
6. I believe it should be accessible at localhost:8080/{name of war folder}

## What I would change if given more time
- Add additional criteria like created and updated timestamps, home gym, etc.
- Creating some security/restrictions to who can create users and search for them.
- It's assumed that in anything like this some level of performance may be necessary so caching (and of course a caching expiration policy) would be necessary.
- Other stuff like this might really just come down to system design, i.e. instances split up regionally? etc.
- The regex for the email could be better.