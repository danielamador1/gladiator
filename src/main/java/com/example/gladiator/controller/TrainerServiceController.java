/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.gladiator.controller;

import com.example.gladiator.entity.Trainer;
import com.example.gladiator.entity.TrainerService;
import java.io.Serializable;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Daniel
 */
@RestController
@RequestMapping("/trainer")
public class TrainerServiceController {
    
    @Autowired
    private TrainerService trainerService;
    
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Trainer getTrainerInfo(@PathVariable Integer id) {
        return trainerService.getTrainerById(id);
    }
    
    @PostMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createTrainer(@RequestBody @Valid TrainerCreationDto trainerDto){
        return trainerService.addTrainer(trainerDto.createTrainer());
    }
}

class TrainerCreationDto implements Serializable {
    @NotEmpty(message = "Email must not be empty")
    @Size(max = 90)
    @Pattern(regexp = "^[^@\\s]+@[^@\\s\\.]+\\.[^@\\.\\s]+$" , message="Invalid email")
    @Setter @Getter private String email;
    
    @NotEmpty(message = "Phone must not be empty")
    @Pattern(regexp="^\\d{10}$", message="Invalid phone/fax format, should be as xxxxxxxxxx")
    @Setter @Getter private String phone;

    @NotEmpty(message = "First Name must not be empty")
    @Size(max = 30)
    @Setter @Getter private String first_name;
    
    @NotEmpty(message = "Last Name must not be empty")
    @Size(max = 45)
    @Setter @Getter private String last_name;
    Trainer trainer;
    
    public Trainer createTrainer(){
        return new Trainer(email, phone, first_name, last_name);
    }
}