/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.gladiator.entity;

import com.example.gladiator.exception.TrainerNotFoundException;
import com.example.gladiator.repository.TrainerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 *
 * @author Daniel
 */
@Service
public class TrainerService {
    
    @Autowired
    private TrainerRepository trainerRepo;
    
    public Trainer getTrainerById(Integer id){
        return trainerRepo.findById(id).orElseThrow(() -> new TrainerNotFoundException(id));
    }
    
    public ResponseEntity<String> addTrainer(Trainer trainer){
        trainerRepo.save(trainer);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
