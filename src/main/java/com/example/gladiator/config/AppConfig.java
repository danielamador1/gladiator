/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.gladiator.config;

import java.util.Properties;
import javax.sql.DataSource;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

/**
 *
 * @author Daniel
 */
public class AppConfig {
    @Bean
    public DataSource dataSource() {
          DriverManagerDataSource dataSource = new DriverManagerDataSource();

          dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
          dataSource.setUsername("root");
          dataSource.setPassword("password");
          dataSource.setUrl(
            "jdbc:mysql://localhost:3306/gladiator?createDatabaseIfNotExist=true"); 

          return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
          LocalContainerEntityManagerFactoryBean factory =
                  new LocalContainerEntityManagerFactoryBean();
          factory.setDataSource(dataSource());
          factory.setPersistenceProviderClass(HibernatePersistenceProvider.class);
          Properties properties = new Properties();
//          properties.setProperty("javax.persistence.schema-generation.database.action", "create");
          factory.setJpaProperties(properties);
          return factory;
    }      
}
